all:

install:
	install -D etc/udev/rules.d/91-permissions.rules $(DESTDIR)/etc/udev/rules.d/91-permissions.rules
	install -D etc/unbound/unbound.conf.d/jk.conf $(DESTDIR)/etc/unbound/unbound.conf.d/jk.conf
	install -D etc/network/interfaces.d/tap1 $(DESTDIR)/etc/network/interfaces.d/tap1
	install -D etc/network/interfaces.d/eth0 $(DESTDIR)/etc/network/interfaces.d/eth0
	install -D etc/network/interfaces.d/wlan0-template $(DESTDIR)/etc/network/interfaces.d/wlan0-template
	install -D etc/sudoers.d/poweroff $(DESTDIR)/etc/sudoers.d/poweroff
	install -D etc/sudoers.d/containerd $(DESTDIR)/etc/sudoers.d/containerd
	install -D etc/sysctl.d/99-dmesg-access.conf $(DESTDIR)/etc/sysctl.d/99-dmesg-access.conf

.PHONY: install all
